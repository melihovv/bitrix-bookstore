<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><? $APPLICATION->ShowTitle(); ?></title>
    <? $APPLICATION->ShowHead(); ?>

    <? $APPLICATION->SetAdditionalCSS("/node_modules/bootstrap/dist/css/bootstrap.css") ?>
    <? $APPLICATION->SetAdditionalCSS("/local/templates/empty/style.css") ?>
</head>
<body>
<? $APPLICATION->ShowPanel(); ?>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Магазин книг</a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/books">Книги</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="/contacts">Обратная связь</a></li>
                </ul>

                <form class="navbar-form navbar-right" action="/search">
                    <div class="form-group">
                        <input type="text" class="form-control"
                               placeholder="Поиск по сайту" name="q">
                    </div>

                    <button type="submit" class="btn btn-default">Искать</button>
                </form>
            </div>
        </div>
    </nav>
