<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
    foreach($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
    ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <?=bitrix_sessid_post()?>

    <div class="form-group">
        <label for="email" class="control-label">
            Адрес электронной почты
            <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        </label>
        <input type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" id="email" class="form-control">
    </div>

    <div class="form-group">
        <label for="amount" class="control-label">
            Количество
            <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("AMOUNT", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        </label>
        <input name="AMOUNT" id="amount" class="form-control" type="number" value="1" min="1">
    </div>

    <?if($arParams["USE_CAPTCHA"] == "Y"):?>
    <div class="form-group">
        <label for="captcha_sid" class="control-label"><?=GetMessage("MFT_CAPTCHA")?></label>
        <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>" class="form-control" id="captcha_sid">

        <br><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"><br>

        <label for="captcha_word" class="control-label"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></label>
        <input type="text" name="captcha_word" size="30" maxlength="50" value="" id="captcha_word" class="form-control">
    </div>
    <?endif;?>

    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="btn btn-primary">
</form>
</div>