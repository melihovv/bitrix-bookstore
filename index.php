<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
?>

<div class="jumbotron">
    <h1>Магазин книг</h1>
    <p>Здесь вы можете найти и заказать интересущие вас книги</p>
    <p><a class="btn btn-primary btn-lg" href="/books" role="button">В каталог книг</a></p>
</div>

<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
?>
