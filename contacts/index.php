<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
$APPLICATION->SetTitle('Обратная связь');
?>

<? $APPLICATION->IncludeComponent("bitrix:main.feedback", "feedback", [
    "EMAIL_TO" => "amelihovv@ya.ru",
    // E-mail, на который будет отправлено письмо
    "EVENT_MESSAGE_ID" => "",
    // Почтовые шаблоны для отправки письма
    "OK_TEXT" => "Спасибо, ваше сообщение принято.",
    // Сообщение, выводимое пользователю после отправки
    "REQUIRED_FIELDS" => [    // Обязательные поля для заполнения
        0 => "EMAIL",
        1 => "MESSAGE",
        2 => "NAME",
    ],
    "USE_CAPTCHA" => "Y",
    // Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
],
    false
); ?>

<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
?>
